#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "GPG_PASSPHRASE"

_DUMMY_FILE_PATH=$(mktemp)

touch "${_DUMMY_FILE_PATH}"
sh "${_CIH_PATH}/utils/get_secret_variable.sh" "GPG_PASSPHRASE" | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s "${_DUMMY_FILE_PATH}"
rm -rf "${_DUMMY_FILE_PATH}"
