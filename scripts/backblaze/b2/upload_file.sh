#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "BACKBLAZE_B2_BUCKET_NAME"
sh "${_CIH_PATH}/utils/require_variable.sh" "BACKBLAZE_B2_LOCAL_FILE_PATH"
sh "${_CIH_PATH}/utils/require_variable.sh" "BACKBLAZE_B2_BUCKET_FILE_PATH"

b2 upload_file --noProgress "${BACKBLAZE_B2_BUCKET_NAME}" "${BACKBLAZE_B2_LOCAL_FILE_PATH}" "${BACKBLAZE_B2_BUCKET_FILE_PATH}" | grep "URL by file name" | awk -F":" '{print $2":"$3}' | tr -d ' '
