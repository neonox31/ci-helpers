#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "BACKBLAZE_B2_ACCOUNT_ID"
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "BACKBLAZE_B2_APPLICATION_KEY"

b2 authorize_account "${BACKBLAZE_B2_ACCOUNT_ID}" "${BACKBLAZE_B2_APPLICATION_KEY}"
