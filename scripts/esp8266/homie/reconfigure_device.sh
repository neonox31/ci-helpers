#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_HOMIE_CONFIG_PATH"

if [ -f "${ESP8266_HOMIE_CONFIG_PATH}" ]; then
  export MQTT_HOST=$(cat "${ESP8266_HOMIE_CONFIG_PATH}" | jq -r '.mqtt.host')
  export ESP8266_HOMIE_MQTT_BASE_TOPIC=$(cat "${ESP8266_HOMIE_CONFIG_PATH}" | jq -r '.mqtt.base_topic')
  export ESP8266_HOMIE_DEVICE_ID=$(cat "${ESP8266_HOMIE_CONFIG_PATH}" | jq -r '.device_id')
else
  ${CIH_PATH}/utils/log/error.sh "${ESP8266_HOMIE_CONFIG_PATH} file doesn't exist."
fi

sh "${_CIH_PATH}/utils/require_variable.sh" "MQTT_HOST"
sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_HOMIE_MQTT_BASE_TOPIC"
sh "${_CIH_PATH}/utils/require_variable.sh" "ESP8266_HOMIE_DEVICE_ID"

mosquitto_pub -h "${MQTT_HOST}" -t "${ESP8266_HOMIE_BASE_TOPIC}/${ESP8266_HOMIE_DEVICE_ID}/\$implementation/config/set" -f "${ESP8266_HOMIE_CONFIG_PATH}"
