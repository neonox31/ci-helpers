#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_PACKAGE_JSON_PATH=${PACKAGE_JSON_PATH:-$PWD/package.json}
_NPM_PUBLISH_ACCESS=${NPM_PUBLISH_ACCESS:-restricted}

npm publish --access "${_NPM_PUBLISH_ACCESS}" "$(dirname "${_PACKAGE_JSON_PATH}")"
