#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "NPM_TOKEN"

_NPMRC_PATH=${NPMRC_PATH:-~/.npmrc}

printf "%s\n" "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > "${_NPMRC_PATH}"
