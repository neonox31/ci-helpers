#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

# shellcheck disable=SC2034
_LEVEL_INFO_MASK=0x1
# shellcheck disable=SC2034
_LEVEL_LOW_MASK=0x2
# shellcheck disable=SC2034
_LEVEL_MODERATE_MASK=0x4
# shellcheck disable=SC2034
_LEVEL_HIGH_MASK=0x8
# shellcheck disable=SC2034
_LEVEL_CRITICAL_MASK=0x16

_THRESHOLD=${YARN_AUDIT_THRESHOLD:-HIGH}
_MAX_LINES=${YARN_AUDIT_MAX_LINES:-3000}
_GROUPS=${YARN_AUDIT_GROUPS:-dependencies}

set +e
if [ -n "${YARN_AUDIT_JSON_REPORT_PATH}" ]; then
  mkdir -p "$(dirname "${YARN_AUDIT_JSON_REPORT_PATH}")"
  yarn audit --groups "${_GROUPS}" --json | yarn gitlab-ci-yarn-audit-parser -o "${YARN_AUDIT_JSON_REPORT_PATH}"
fi

if [ -n "${YARN_AUDIT_HTML_REPORT_PATH}" ]; then
  mkdir -p "$(dirname "${YARN_AUDIT_HTML_REPORT_PATH}")"
  yarn audit --groups "${_GROUPS}" --json | yarn yarn-audit-html --output "${YARN_AUDIT_HTML_REPORT_PATH}"
fi

_AUDIT_OUTPUT=$(yarn audit --groups "${_GROUPS}")
_AUDIT_STATUS=$?
set -e

if [ "$(echo "${_AUDIT_OUTPUT}" | wc -l)" -gt "${_MAX_LINES}" ]; then
  echo "${_AUDIT_OUTPUT}" | head -n "${_MAX_LINES}"
  echo "..."
  sh "${_CIH_PATH}/utils/log/warn.sh" "Audit report output was intentionally limited to ${_MAX_LINES} lines, to get full list please look at JSON or HTML reports."
else
  echo "${_AUDIT_OUTPUT}"
fi

if [ -n "$(eval echo \$"_LEVEL_${_THRESHOLD}_MASK")" ]; then
        _THRESHOLD_MASK=$(printf "%s" "$(eval printf "%s" "\"\$_LEVEL_${_THRESHOLD}_MASK\"")")
        if [ $(( _AUDIT_STATUS & _THRESHOLD_MASK )) -ne 0 ]; then
          exit 2
        fi
else
  sh "${_CIH_PATH}/utils/log/error.sh" "${_THRESHOLD} threshold is unknown."
fi
