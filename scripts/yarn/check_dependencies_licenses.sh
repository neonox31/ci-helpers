#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CSV_PATH=${YARN_DEPENDENCIES_CSV_REPORT_PATH:-dependencies.csv}
_JSON_PATH=${YARN_DEPENDENCIES_JSON_REPORT_PATH:-dependencies.json}

yarn license-checker --direct --production --csv --out "${_CSV_PATH}"
yarn license-checker --direct --production --json --out "${_JSON_PATH}"
