# Artifactory helpers scripts

- [artifactory/auth_with_npm.sh](#artifactoryauth_with_npmsh)
- [artifactory/add_tag_to_docker_image.sh](#artifactoryadd_tag_to_docker_imagesh)

## artifactory/auth_with_npm.sh

Authenticate [NPM](https://www.npmjs.com/) with [Artifactory](https://jfrog.com/artifactory/).

### Dependencies

- base64
- curl
- npm

### Environment variables

| Variable                                           | Required | Description                           |
|:---------------------------------------------------|:---------|:--------------------------------------|
| `ARTIFACTORY_TOKEN` or <br>`ARTIFACTORY_TOKEN_B64` | Yes      | Artifactory API key                   |
| `ARTIFACTORY_URL`                                  | Yes      | Artifactory URL                       |
| `ARTIFACTORY_NPM_REPO`                             | Yes      | Artifactory NPM repo key              |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    ARTIFACTORY_URL: "https://my.artifactory.com"
    ARTIFACTORY_NPM_REPO: "npm-local"
  script:
    - ${CIH_PATH}/artifactory/auth_with_npm.sh
```

## artifactory/add_tag_to_docker_image.sh

Add a tag to an existing [Artifactory](https://jfrog.com/artifactory/) docker image.

### Dependencies

- base64
- curl

### Environment variables

| Variable                                           | Required | Description                           |
|:---------------------------------------------------|:---------|:--------------------------------------|
| `ARTIFACTORY_TOKEN` or <br>`ARTIFACTORY_TOKEN_B64` | Yes      | Artifactory API key                   |
| `ARTIFACTORY_URL`                                  | Yes      | Artifactory URL                       |
| `ARTIFACTORY_DOCKER_IMAGE`                         | Yes      | Artifactory docker image              |
| `ARTIFACTORY_DOCKER_REPO`                          | Yes      | Artifactory docker repo key           |
| `ARTIFACTORY_DOCKER_SRC_TAG`                       | Yes      | Artifactory docker source tag         |
| `ARTIFACTORY_DOCKER_DEST_TAG`                      | Yes      | Artifactory docker destination tag    |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    ARTIFACTORY_URL: "https://my.artifactory.com"
    ARTIFACTORY_DOCKER_IMAGE: "nginx"
    ARTIFACTORY_DOCKER_REPO: "docker-local"
    ARTIFACTORY_DOCKER_SRC_TAG: "${CI_COMMIT_SHA}"
    ARTIFACTORY_DOCKER_DEST_TAG: "1.0.0"
  script:
    - ${CIH_PATH}/artifactory/add_tag_to_docker_image.sh
```
