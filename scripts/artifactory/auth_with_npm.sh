#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "ARTIFACTORY_TOKEN"
sh "${_CIH_PATH}/utils/require_variable.sh" "ARTIFACTORY_URL"
sh "${_CIH_PATH}/utils/require_variable.sh" "ARTIFACTORY_NPM_REPO"

curl -qsSf \
     -X GET "${ARTIFACTORY_URL}/artifactory/api/npm/auth" \
     -H "X-Api-Key: $(sh "${_CIH_PATH}/utils/get_secret_variable.sh" "ARTIFACTORY_TOKEN")" \
> "${NPM_RC_PATH:-$HOME/.npmrc}"

_REGISTRY="${ARTIFACTORY_URL}/artifactory/api/npm/${ARTIFACTORY_NPM_REPO}/"

npm config set registry "${_REGISTRY}"

if command -v "yarn" > /dev/null; then
  yarn -s config set registry "${_REGISTRY}"
fi
