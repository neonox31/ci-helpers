#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "ARTIFACTORY_URL"
sh "${_CIH_PATH}/utils/require_variable.sh" "ARTIFACTORY_NPM_REPO"
sh "${_CIH_PATH}/utils/require_variable.sh" "PACKAGE_JSON_NAME"
sh "${_CIH_PATH}/utils/require_variable.sh" "PACKAGE_JSON_VERSION"

_NPM_RC_PATH=${NPMRC_PATH:-~/.npmrc}

if [ ! -f "${_NPM_RC_PATH}" ]; then
  sh "${_CIH_PATH}/utils/log/error.sh" "${_NPM_RC_PATH} doesn't exists. Please authenticate before."
fi

curl -qsSf \
     -X DELETE "${ARTIFACTORY_URL}/artifactory/api/npm/${ARTIFACTORY_NPM_REPO}/${PACKAGE_JSON_NAME}/-/${PACKAGE_JSON_NAME}-${PACKAGE_JSON_VERSION}.tgz/-rev/1-0" \
     -u "$(grep "_auth" < "${_NPM_RC_PATH}" | cut -d'=' -f2- | sed 's/"//g' | base64 -d)"
