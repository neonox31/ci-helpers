#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GITLAB_RELEASE_TAG"

set -- release-cli create --tag-name "${GITLAB_RELEASE_TAG}"

# shellcheck disable=SC2153
if [ -n "${GITLAB_RELEASE_NAME}" ]; then
  set -- "$@" --name "${GITLAB_RELEASE_NAME}"
fi

# shellcheck disable=SC2153
if [ -n "${GITLAB_RELEASE_DESCRIPTION}" ]; then
  set -- "$@" --description "${GITLAB_RELEASE_DESCRIPTION}"
fi

# shellcheck disable=SC2153
if [ -n "${GITLAB_RELEASE_ASSETS}" ]; then
  while IFS= read -r line; do
    _ASSET_NAME=$(printf '%s' "${line}" | awk -F '|' '{ print $1 }')
    _ASSET_URL=$(printf '%s' "${line}" | awk -F '|' '{ print $2 }')
    set -- "$@" --assets-link "{\"name\":\"${_ASSET_NAME}\",\"url\":\"${_ASSET_URL}\"}"
  done << EOF
$(printf '%b' "${GITLAB_RELEASE_ASSETS}")
EOF
fi

"$@"
