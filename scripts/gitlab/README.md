# Gitlab helpers scripts

- [gitlab/create_release.sh](#gitlabcreate_releasesh)

## gitlab/create_release.sh

Create a release using [Gitlab API](https://docs.gitlab.com/ee/api/releases/)

### Dependencies

- base64
- curl

### Environment variables

| Variable                                 | Required | Description                                                                                                                                                          |
|:-----------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `GITLAB_TOKEN` or <br>`GITLAB_TOKEN_B64` | Yes      | Gitlab personal access token.<br> For creating one, see [here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) |
| `CI_SERVER_HOST`                         | Yes      | Gitlab host URL.<br> Already defined in runner [predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)                             |
| `CI_PROJECT_ID`                          | Yes      | Gitlab project ID.<br> Already defined in runner [predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)                           |
| `GITLAB_RELEASE_NAME`                    | Yes      | Release name                                                                                                                                                         |
| `GITLAB_RELEASE_TAG_NAME`                | Yes      | The tag where the release will be created from                                                                                                                       |
| `GITLAB_RELEASE_DESCRIPTION`             | Yes      | The description of the release. You can use Markdown.                                                                                                                |
| `GITLAB_RELEASE_LINK1`                   | No       | Release first link.<br>Link name and link URL should be separated by a pipe                                                                                          |
| `GITLAB_RELEASE_LINK2`                   | No       | Release second link.<br>Link name and link URL should be separated by a pipe                                                                                         |
| `GITLAB_RELEASE_LINK3`                   | No       | Release third link.<br>Link name and link URL should be separated by a pipe                                                                                          |

⚠ Release links are limited to a number of 3.

### Arguments

_Script has no arguments_

### Output

_Script forward docker output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GITLAB_RELEASE_NAME: "My first release!"
    GITLAB_RELEASE_TAG_NAME: "1.0.0"
    GITLAB_RELEASE_DESCRIPTION: "Yeah !"
    GITLAB_RELEASE_LINK1: "Changelog|https://my.app/CHANGELOG.md"
    GITLAB_RELEASE_LINK2: "README|https://my.app/README.md"
  script:
    - ${CIH_PATH}/gitlab/create_release.sh
```
