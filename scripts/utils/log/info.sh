#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

now=$(date --utc "${CIH_LOG_DATE_FORMAT:-+%FT%TZ}")

printf "\e[90m%s\e[0m \e[94m[INFO]\e[0m %s\n" "${now}" "${1}"
