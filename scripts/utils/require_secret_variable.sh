#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

var_name="${1}"
encoded_var_name="${1}_B64"
masked_var_pattern="^[[:alpha:][:digit:]\/\+\=\@\:\_\-]{8,}\$"

if [ -z "$(eval echo \$"${var_name}")" ] && [ -z "$(eval echo \$"${encoded_var_name}")" ]; then
    sh "${_CIH_PATH}/utils/log/error.sh" "${var_name} or ${encoded_var_name} environment variable should be defined"
fi

if [ -z "$(eval echo \$"${var_name}")" ] && [ -n "$(eval echo \$"${encoded_var_name}")" ]; then
    if ! eval echo \$"${encoded_var_name}" | grep -qE "${masked_var_pattern}"; then
        sh "${_CIH_PATH}/utils/log/error.sh" "${encoded_var_name} environment variable value should be maskable by Gitlab. More information here : https://code.webfactory.intelligence-airbusds.com/ci-centre/ci-helpers#note-about-secret-variables"
    fi
    exit 0;
fi

if [ -n "$(eval echo \$"${var_name}")" ] && [ -n "$(eval echo \$"${encoded_var_name}")" ]; then
    sh "${_CIH_PATH}/utils/log/warn.sh" "both ${var_name} and ${encoded_var_name} environment variables are defined, ${encoded_var_name} will be used by default."
    exit 0;
fi

lines_count=$(eval echo \$"${var_name}" | wc -l)

if [ "${lines_count}" -gt 1 ] || ! eval echo \$"${var_name}" | grep -qE "${masked_var_pattern}"; then
    sh "${_CIH_PATH}/utils/log/warn.sh" "${var_name} environment variable can't be masked by Gitlab. Consider replace it by the ${encoded_var_name} environment variable with base64 encoded value due to safety reasons. More information here: https://code.webfactory.intelligence-airbusds.com/ci-centre/ci-helpers#note-about-secret-variables"
fi
