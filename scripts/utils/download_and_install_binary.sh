#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

"${_CIH_PATH}/utils/require_variable.sh" "BIN_DOWNLOAD_URL"
"${_CIH_PATH}/utils/require_variable.sh" "BIN_INSTALL_PATH"

if command -v "curl"; then
  curl -L "${BIN_DOWNLOAD_URL}" -o "${BIN_INSTALL_PATH}"
elif command -v "wget"; then
  wget -qO "${BIN_INSTALL_PATH}" "${DOWNLOAD_URL}"
fi

chmod +x "${BIN_INSTALL_PATH}"
