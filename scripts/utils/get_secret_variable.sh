#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

var_name="${1}"
encoded_var_name="${1}_B64"

if [ -n "$(eval echo \$"${encoded_var_name}")" ]; then
    printf "%s" "$(eval printf "%s" "\"\$${encoded_var_name}\"")" | base64 -d
else
    printf "%s" "$(eval printf "%s" "\"\$${var_name}\"")"
fi
