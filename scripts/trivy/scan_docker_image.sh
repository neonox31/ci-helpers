#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
"${_CIH_PATH}/utils/require_variable.sh" "DOCKER_IMAGE"

if [ -z "${TRIVY_AUTH_URL}" ] && [ -n "${DOCKER_REGISTRY}" ]; then
   export TRIVY_AUTH_URL="${DOCKER_REGISTRY}"
fi

if [ -z "${TRIVY_USERNAME}" ] && [ -n "${DOCKER_USER}" ]; then
    export TRIVY_USERNAME="${DOCKER_USER}"
fi

if [ -z "${TRIVY_PASSWORD}" ] && { [ -n "${DOCKER_PASSWORD}" ] || [ -n "${DOCKER_PASSWORD_B64}" ]; }; then
   TRIVY_PASSWORD="$(sh "${_CIH_PATH}/utils/get_secret_variable.sh" "DOCKER_PASSWORD")"
   export TRIVY_PASSWORD
fi

if [ -n "${TRIVY_SEVERITY}" ]; then
  _SEVERITY=${TRIVY_SEVERITY}
  unset "TRIVY_SEVERITY"
else
  _SEVERITY="MEDIUM,HIGH,CRITICAL"
fi

_TRIVY_GITLAB_REPORT_PATH="${TRIVY_GITLAB_REPORT_PATH:-gl-container-scanning-report.json}"

mkdir -p "$(dirname "${TRIVY_GITLAB_REPORT_PATH}")"

_TRIVY_DOCKER_PATH="${DOCKER_REGISTRY:-}${DOCKER_REGISTRY:+/}${DOCKER_IMAGE}:${DOCKER_TAG:-latest}"

trivy --no-progress --timeout 20m --format template --template "@/contrib/gitlab.tpl" -o "${_TRIVY_GITLAB_REPORT_PATH}" "${_TRIVY_DOCKER_PATH}"
trivy --no-progress --timeout 20m "${_TRIVY_DOCKER_PATH}"
trivy --no-progress --timeout 20m --severity ${_SEVERITY} --exit-code 2 "${_TRIVY_DOCKER_PATH}" > /dev/null
