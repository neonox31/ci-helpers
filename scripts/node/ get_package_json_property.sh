#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}

_PACKAGE_JSON_PATH=${PACKAGE_JSON_PATH:-$PWD/package.json}

if [ -z "${1}" ]; then
  sh "${_CIH_PATH}/utils/log/error.sh" "a package.json property should be defined."
fi

if [ ! -f "${_PACKAGE_JSON_PATH}" ]; then
  sh "${_CIH_PATH}/utils/log/error.sh" "${_PACKAGE_JSON_PATH} file not found."
fi

cat < "${_PACKAGE_JSON_PATH}" \
  | grep "${1}" \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | awk '{$1=$1};1'
