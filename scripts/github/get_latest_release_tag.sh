#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GITHUB_REPOSITORY"

curl -s "https://api.github.com/repos/${GITHUB_REPOSITORY}/releases/latest" | sed -n 's/.*tag_name":\s"\(.*\)".*/\1/p' | head -1
