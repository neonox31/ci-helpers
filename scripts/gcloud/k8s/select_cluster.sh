#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/../.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_PROJECT"
sh "${_CIH_PATH}/utils/require_variable.sh" "K8S_CLUSTER"
sh "${_CIH_PATH}/utils/require_variable.sh" "K8S_CLUSTER_ZONE"

gcloud container clusters get-credentials "${K8S_CLUSTER}" \
                                          --zone "${K8S_CLUSTER_ZONE}" \
                                          --project "${GCLOUD_PROJECT}"
