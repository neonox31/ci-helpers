#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCR_SRC_TAG"
sh "${_CIH_PATH}/utils/require_variable.sh" "GCR_DEST_TAG"

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

gcloud container images add-tag "${GCR_SRC_TAG}" \
                                "${GCR_DEST_TAG}"
