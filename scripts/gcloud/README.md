# Gcloud helpers scripts

- [gcloud/auth_with_service_account.sh](#gcloudauth_with_service_accountsh)
- [gcloud/get_ip_address.sh](#gcloudget_ip_addresssh)
- [gcloud/release_ip_address.sh](#gcloudrelease_ip_addresssh)
- [gcloud/reserve_ip_address.sh](#gcloudreserve_ip_addresssh)
- gcloud/appengine/
    - [gcloud/appengine/delete_version.sh](#gcloudappenginedelete_versionsh)
    - [gcloud/appengine/deploy_version.sh](#gcloudappenginedeploy_versionsh)
    - [gcloud/appengine/is_service_exists.sh](#gcloudappengineis_service_existssh)
    - [gcloud/appengine/is_version_exists.sh](#gcloudappengineis_version_existssh)
    - [gcloud/appengine/is_version_has_traffic.sh](#gcloudappengineis_version_has_trafficsh)
    - [gcloud/appengine/promote_version.sh](#gcloudappenginepromote_versionsh)
- gcloud/docker/
     - [gcloud/docker/add_tag.sh](#gclouddockeradd_tagsh)
     - [gcloud/docker/build_image.sh](#gclouddockerbuild_imagesh)
- gcloud/k8s/
     - [gcloud/k8s/select_cluster.sh](#gcloudk8sselect_clustersh)

## gcloud/auth_with_service_account.sh

Authenticate on [Google Cloud](https://cloud.google.com/) with a service account.

### Dependencies

- base64
- gcloud

### Environment variables

| Variable                              | Required | Description                           |
|:--------------------------------------|:---------|:--------------------------------------|
| `GCLOUD_KEY` or <br>`GCLOUD_KEY_B64`  | Yes      | Google Cloud service account JSON key |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  script:
    - ${CIH_PATH}/gcloud/auth_with_service_account.sh
```

## gcloud/get_ip_address.sh

Print an existing global or regional [IP address](https://cloud.google.com/compute/docs/ip-addresses) on [Google Cloud](https://cloud.google.com/).

### Dependencies

- gcloud

### Environment variables

| Variable                  | Required | Description                                                                                                                                  |
|:--------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`          | Yes      | Google cloud [project](https://cloud.google.com/resource-manager/docs/creating-managing-projects) id                                         |
| `GCLOUD_IP_ADDRESS_NAME`  | Yes      | IP address name                                                                                                                              |
| `GCLOUD_IP_ADDRESS_REGION`| No       | IP address Google cloud [region](https://cloud.google.com/compute/docs/regions-zones)<br>If variable is unset, global resource will be used. |

### Arguments

_Script has no arguments_

### Output

Desired IP address (i.e `35.100.120.90`)

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GCLOUD_IP_ADDRESS_NAME: "my-ip-name"
    GCLOUD_IP_ADDRESS_ZONE: "europe-west1"
  script:
    - ${CIH_PATH}/gcloud/get_ip_address.sh
```

## gcloud/release_ip_address.sh

Release an existing global or regional [IP address](https://cloud.google.com/compute/docs/ip-addresses) on [Google Cloud](https://cloud.google.com/).

### Dependencies

- gcloud

### Environment variables

| Variable                  | Required | Description                                                                                                                                  |
|:--------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`          | Yes      | Google cloud [project](https://cloud.google.com/resource-manager/docs/creating-managing-projects) id                                         |
| `GCLOUD_IP_ADDRESS_NAME`  | Yes      | IP address name                                                                                                                              |
| `GCLOUD_IP_ADDRESS_REGION`| No       | IP address Google cloud [region](https://cloud.google.com/compute/docs/regions-zones)<br>If variable is unset, global resource will be used. |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GCLOUD_IP_ADDRESS_NAME: "my-ip-name"
    GCLOUD_IP_ADDRESS_ZONE: "europe-west1"
  script:
    - ${CIH_PATH}/gcloud/release_ip_address.sh
```

## gcloud/reserve_ip_address.sh

Reserve a new global or regional [IP address](https://cloud.google.com/compute/docs/ip-addresses) on [Google Cloud](https://cloud.google.com/).

### Dependencies

- gcloud

### Environment variables

| Variable                  | Required | Description                                                                                                                                  |
|:--------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`          | Yes      | Google cloud [project](https://cloud.google.com/resource-manager/docs/creating-managing-projects) id                                         |
| `GCLOUD_IP_ADDRESS_NAME`  | Yes      | IP address name                                                                                                                              |
| `GCLOUD_IP_ADDRESS_REGION`| No       | IP address Google cloud [region](https://cloud.google.com/compute/docs/regions-zones)<br>If variable is unset, global resource will be used. |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GCLOUD_IP_ADDRESS_NAME: "my-ip-name"
    GCLOUD_IP_ADDRESS_ZONE: "europe-west1"
  script:
    - ${CIH_PATH}/gcloud/reserve_ip_address.sh
```

## gcloud/appengine/delete_version.sh

Delete an existing version using
[Google AppEngine](https://cloud.google.com/appengine) service.

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                             |
|:-----------------------|:---------|:----------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`       | Yes      | Google Cloud project id                                                                 |
| `GAE_VERSION`          | Yes      | Google AppEngine version name                                                           |
| `GAE_SERVICE`          | No       | Google AppEngine service name.<br>If variable is unset, `default` service will be used. |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GAE_VERSION: "1-0-0"
    GAE_DEPLOYABLES_PATH: "dist/"
  script:
    - ${CIH_PATH}/gcloud/appengine/delete_version.sh
```

## gcloud/appengine/deploy_version.sh

Deploy a local application using
[Google AppEngine](https://cloud.google.com/appengine) service.

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                             |
|:-----------------------|:---------|:----------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`       | Yes      | Google Cloud project id                                                                 |
| `GAE_VERSION`          | Yes      | Google AppEngine version name                                                           |
| `GAE_DEPLOYABLES_PATH` | Yes      | Local application path (containing `app.yaml` file)                                     |
| `GAE_SERVICE`          | No       | Google AppEngine service name.<br>If variable is unset, `default` service will be used. |
| `GAE_PROMOTE_VERSION`  | No       | Migrate traffic on version after deploy                                                 |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GAE_VERSION: "1-0-0"
    GAE_DEPLOYABLES_PATH: "dist/"
  script:
    - ${CIH_PATH}/gcloud/appengine/deploy_version.sh
```

## gcloud/appengine/is_service_exists.sh

Check if a service exists on [Google AppEngine](https://cloud.google.com/appengine).

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                             |
|:-----------------------|:---------|:----------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`       | Yes      | Google Cloud project id                                                                 |
| `GAE_SERVICE`          | Yes      | Google AppEngine service name                                                           |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success (true)   |
| `> 0`  | Failure (false)  |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GAE_SERVICE: "my-service"
  script:
    - |
      if ${CIH_PATH}/gcloud/appengine/is_service_exists.sh; then
        echo "Service ${GAE_SERVICE} exists !"
      else
        echo "Service ${GAE_SERVICE} not found"
```

## gcloud/appengine/is_version_exists.sh

Check if a version exists on [Google AppEngine](https://cloud.google.com/appengine).

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                             |
|:-----------------------|:---------|:----------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`       | Yes      | Google Cloud project id                                                                 |
| `GAE_VERSION`          | Yes      | Google AppEngine version name                                                           |
| `GAE_SERVICE`          | No       | Google AppEngine service name.<br>If variable is unset, `default` service will be used. |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success (true)   |
| `> 0`  | Failure (false)  |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GAE_SERVICE: "my-service"
    GAE_VERSION: "my-version"
  script:
    - |
      if ${CIH_PATH}/gcloud/appengine/is_version_exists.sh; then
        echo "Version ${GAE_VERSION} exists !"
      else
        echo "Version ${GAE_VERSION} not found"
```

## gcloud/appengine/is_version_has_traffic.sh

Check if a version has traffic on [Google AppEngine](https://cloud.google.com/appengine).

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                             |
|:-----------------------|:---------|:----------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`       | Yes      | Google Cloud project id                                                                 |
| `GAE_VERSION`          | Yes      | Google AppEngine version name                                                           |
| `GAE_SERVICE`          | No       | Google AppEngine service name.<br>If variable is unset, `default` service will be used. |

### Arguments

_Script has no arguments_

### Output

_Script has no output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success (true)   |
| `> 0`  | Failure (false)  |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GAE_SERVICE: "my-service"
    GAE_VERSION: "my-version"
  script:
    - |
      if ${CIH_PATH}/gcloud/appengine/is_version_has_traffic.sh; then
        echo "Version ${GAE_VERSION} has traffic !"
      else
        echo "Version ${GAE_VERSION} has no traffic"
```

## gcloud/appengine/promote_version.sh

Promote traffic on specific version on [Google AppEngine](https://cloud.google.com/appengine).

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                             |
|:-----------------------|:---------|:----------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`       | Yes      | Google Cloud project id                                                                 |
| `GAE_VERSION`          | Yes      | Google AppEngine version name                                                           |
| `GAE_SERVICE`          | No       | Google AppEngine service name.<br>If variable is unset, `default` service will be used. |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GAE_SERVICE: "my-service"
    GAE_VERSION: "my-version"
  script:
    - ${CIH_PATH}/gcloud/appengine/promote_version.sh
```

## gcloud/docker/add_tag.sh

Add a new tag from an existing one on [Google cloud container registry](https://cloud.google.com/container-registry).

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                             |
|:-----------------------|:---------|:----------------------------------------------------------------------------------------|
| `GCR_SRC_TAG`          | Yes      | Google container registry source tag.<br>e.g. `eu.gcr.io/foo/bar:1.0.0-0`               |
| `GCR_DEST_TAG`         | Yes      | Google container registry destination tag.<br>e.g. `eu.gcr.io/foo/bar:1.0.0`            |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCR_SRC_TAG: "${CI_COMMIT_SHA}"
    GCR_DEST_TAG: "1.0.0"
  script:
    - ${CIH_PATH}/gcloud/docker/add_tag.sh
```

## gcloud/docker/build_image.sh

Remotely build a docker image using [Google cloud build](https://cloud.google.com/cloud-build) and push it on [Google cloud container registry](https://cloud.google.com/container-registry).

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                      |
|:-----------------------|:---------|:---------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`       | Yes      | Google Cloud project id                                                          |
| `GCR_TAG`              | Yes      | Google container registry destination tag.<br>e.g. `eu.gcr.io/foo/bar:1.0.0`     |
| `GCR_SOURCE`           | No       | Source folder path for building image.<br> Defaults to current working directory |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    GCR_TAG: "eu.gcr.io/${GCLOUD_PROJECT}/foo:1.0.0"
    GCR_SOURCE: "docker/"
  script:
    - ${CIH_PATH}/gcloud/docker/build_image.sh
```

## gcloud/k8s/select_cluster.sh

Fetch credentials for a running [Google kubernetes engine](https://cloud.google.com/kubernetes-engine) cluster.

### Dependencies

- gcloud

### Environment variables

| Variable               | Required | Description                                                                                   |
|:-----------------------|:---------|:----------------------------------------------------------------------------------------------|
| `GCLOUD_PROJECT`       | Yes      | Google Cloud project id                                                                       |
| `K8S_CLUSTER`          | Yes      | Google kubernetes engine cluster name                                                         |
| `K8S_CLUSTER_ZONE`     | Yes      | Google kubernetes engine cluster [zone](https://cloud.google.com/compute/docs/regions-zones). |

### Arguments

_Script has no arguments_

### Output

_Script forwards gcloud output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    GCLOUD_PROJECT: "d-awf-myproject"
    K8S_CLUSTER: "cluster-name"
    K8S_CLUSTER_ZONE: "europe-west1-b"
  script:
    - ${CIH_PATH}/gcloud/k8s/select_cluster.sh
```
