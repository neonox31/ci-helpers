#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/../.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_PROJECT"
sh "${_CIH_PATH}/utils/require_variable.sh" "GAE_SERVICE"

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_SERVICE=$(\
  gcloud app services list --project "${GCLOUD_PROJECT}" \
                           --filter="${GAE_SERVICE}" \
                           --format="value(id)"\
)

if [ -n "${_SERVICE}" ] && [ "${_SERVICE}" = "${GAE_SERVICE}" ]; then
    exit 0
fi
exit 1

