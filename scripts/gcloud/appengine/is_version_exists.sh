#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/../.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_PROJECT"
sh "${_CIH_PATH}/utils/require_variable.sh" "GAE_VERSION"

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_VERSION=$(\
  gcloud app versions list --project "${GCLOUD_PROJECT}" \
                           --service "${GAE_SERVICE:-default}" \
                           --filter="${GAE_VERSION}" \
                           --format="value(id)"\
)

if [ -n "${_VERSION}" ] && [ "${_VERSION}" = "${GAE_VERSION}" ]; then
    exit 0
fi
exit 1

