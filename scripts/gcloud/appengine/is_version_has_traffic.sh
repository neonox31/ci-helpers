#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/../.."}
sh "${_CIH_PATH}/utils/require_variable.sh" "GCLOUD_PROJECT"
sh "${_CIH_PATH}/utils/require_variable.sh" "GAE_VERSION"

_TRAFFIC_SPLIT=$(\
  gcloud app versions list --project "${GCLOUD_PROJECT}" \
                           --service "${GAE_SERVICE:-default}" \
                           --filter "${GAE_VERSION}" \
                           --hide-no-traffic \
                           --format='value(traffic_split)'\
)

if [ "${_TRAFFIC_SPLIT}" = "1.00" ]; then
   exit 0
fi
exit 1
