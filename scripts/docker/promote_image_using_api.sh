#!/bin/sh

set -e

[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

get_token()
{
  set -e
  _JWT=$(curl -qsSf -k \
                      -u "${DOCKER_USER}:$(sh "${_CIH_PATH}/utils/get_secret_variable.sh" "DOCKER_PASSWORD")" \
                      "${_TOKEN_ENDPOINT}?service=${_SERVICE}&scope=repository:${1}:${2}" \
  )
  echo "${_JWT}" | tr -d '\n' | sed "s/.*{.*\"token\":\s*\"\([^\"]*\).*}.*/\1/g"
}

_CIH_PATH=${CIH_PATH:="$(dirname "${0}")/.."}
sh "${_CIH_PATH}/utils/require_secret_variable.sh" "DOCKER_PASSWORD"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_USER"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_PROMOTE_SRC_IMAGE"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_PROMOTE_DEST_IMAGE"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_PROMOTE_SRC_TAG"
sh "${_CIH_PATH}/utils/require_variable.sh" "DOCKER_PROMOTE_DEST_TAG"

CONTENT_TYPE="application/vnd.docker.distribution.manifest.v2+json"

_TOKEN_ENDPOINT=${DOCKER_REGISTRY_TOKEN_ENDPOINT:-"https://auth.docker.io/token"}
_SERVICE=${DOCKER_REGISTRY_TOKEN_SERVICE:-"registry.docker.io"}
_REGISTRY=${DOCKER_REGISTRY:-index.docker.io}

_SRC_TOKEN=$(get_token "${DOCKER_PROMOTE_SRC_IMAGE}" "pull")
_DEST_TOKEN=$(get_token "${DOCKER_PROMOTE_DEST_IMAGE}" "pull,push")

_SRC_MANIFEST=$(curl -qsSf -k \
                     -H "Authorization: Bearer ${_SRC_TOKEN}" -H "Accept: ${CONTENT_TYPE}" \
                     "https://${_REGISTRY}/v2/${DOCKER_PROMOTE_SRC_IMAGE}/manifests/${DOCKER_PROMOTE_SRC_TAG}" \
)

curl -qsSf -k \
     -X PUT \
     -H "Authorization: Bearer ${_DEST_TOKEN}" -H "Content-Type: ${CONTENT_TYPE}" \
     -d "${_SRC_MANIFEST}" \
     "https://${_REGISTRY}/v2/${DOCKER_PROMOTE_DEST_IMAGE}/manifests/${DOCKER_PROMOTE_DEST_TAG}"

sh "${_CIH_PATH}/utils/log/info.sh" "successfully promoted ${DOCKER_PROMOTE_SRC_IMAGE}:${DOCKER_PROMOTE_SRC_TAG} image to ${DOCKER_PROMOTE_DEST_IMAGE}:${DOCKER_PROMOTE_DEST_TAG}"
