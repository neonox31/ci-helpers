# Docker helpers scripts

- [docker/auth.sh](#dockerauthsh)

## docker/auth.sh

Authenticate [docker](https://www.docker.com/) with a private registry.

### Dependencies

- docker

### Environment variables

| Variable                                           | Required | Description                           |
|:---------------------------------------------------|:---------|:--------------------------------------|
| `DOCKER_PASSWORD` or <br>`DOCKER_PASSWORD_B64`     | Yes      | Docker registry password              |
| `DOCKER_USER`                                      | Yes      | Docker registry username              |
| `DOCKER_REGISTRY`                                  | Yes      | Docker registry url                   |

### Arguments

_Script has no arguments_

### Output

_Script forward docker output_

### Exit codes

| Code   | Description      |
|:-------|:-----------------|
| `0`    | Success          |
| `> 0`  | Failure          |

### Example

```yaml
include:
  project: "ci-centre/ci-helpers"
  file: "/templates/.gitlab-ci-tpl.yml"
  ref: "1.0.0"

my job:
  variables:
    DOCKER_REGISTRY: "hub.docker.local"
    DOCKER_USER: "foo"
  script:
    - ${CIH_PATH}/docker/auth.sh
```
