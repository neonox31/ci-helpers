load("@ytt:base64", "base64")

def env_file_as_secret_data(content):
  out = {}
  for line in content.splitlines():
    out.update({line.split("=", 1)[0]: base64.encode(line.split("=", 1)[1])})
  end
  return out
end
