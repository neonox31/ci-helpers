#!/bin/sh

set -e
[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

# shellcheck disable=SC2016
find . -type f \( -name "*.enc" -o -name "*.enc.*" \) -print0 | xargs -0 -I '$src_file' sh -c '
dest_file=$(echo $src_file | sed -r "s/\.enc(\.?)/\1/g") &&
echo "decrypting $src_file to $dest_file..." &&
sops -d $src_file > $dest_file
'
