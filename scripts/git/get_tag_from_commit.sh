#!/bin/sh

[ -n "${CIH_DEBUG}" ] && set -x

# --- Specs stubs injection ---
test || __() { :; }
__ begin __
# -----------------------------

_GIT_COMMIT_SHA=${GIT_COMMIT_SHA:-HEAD}

_TAG=$(git describe --exact-match "${_GIT_COMMIT_SHA}" 2> /dev/null)
printf "%s" "${_TAG}"
