# shellcheck shell=sh

Describe 'docker/auth.sh'
  CIH_SCRIPT="${CIH_PATH}/docker/auth.sh"

  set_variables() {
    export DOCKER_PASSWORD="my-password"
    export DOCKER_USER="me"
    export DOCKER_REGISTRY="my-registry"
  }

  Before 'set_variables'

  It 'should fail when DOCKER_PASSWORD env variable is not defined'
    unset DOCKER_PASSWORD

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "DOCKER_PASSWORD or DOCKER_PASSWORD_B64 environment variable should be defined"
  End

  # ---

  It 'should fail when DOCKER_USER env variable is not defined'
    unset DOCKER_USER

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "DOCKER_USER environment variable should be defined"
  End

 # ---

  It 'should success and authenticates on private registry'
    Intercept begin
    __begin__() {
      docker() {
        _INPUT="$(cat)"

        if [ "${_INPUT}" = "${DOCKER_PASSWORD}" ] && [ "${*}" = "login ${DOCKER_REGISTRY} --username ${DOCKER_USER} --password-stdin" ]; then
          echo "success"
        fi
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

 # ---

  It 'should success and authenticates on docker registry'
    Intercept begin
    __begin__() {
      docker() {
        _INPUT="$(cat)"

        if [ "${_INPUT}" = "${DOCKER_PASSWORD}" ] && [ "${*}" = "login  --username ${DOCKER_USER} --password-stdin" ]; then
          echo "success"
        fi
      }
    }

    unset DOCKER_REGISTRY

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on docker error'
    Intercept begin
    __begin__() {
      docker() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End

End
