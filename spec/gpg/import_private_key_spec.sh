# shellcheck shell=sh

Describe 'gpg/import_private_key.sh'
  CIH_SCRIPT="${CIH_PATH}/gpg/import_private_key.sh"

  set_variables() {
    export GPG_PRIVATE_KEY="my-private-key"
  }

  Before 'set_variables'

  It 'should fail when GPG_PRIVATE_KEY env variable is not defined'
    unset GPG_PRIVATE_KEY

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GPG_PRIVATE_KEY or GPG_PRIVATE_KEY_B64 environment variable should be defined"
  End

 # ---

  It 'should success and import secret key'
    Intercept begin
    __begin__() {
      gpg() {
        _INPUT="$(cat)"

        if [ "${_INPUT}" = "${GPG_PRIVATE_KEY}" ] && [ "${*}" = "--batch --import" ]; then
          echo "success"
        fi
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

 # ---

  It 'should fail on gpg error'
    Intercept begin
    __begin__() {
      gpg() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End

End
