# shellcheck shell=sh

Describe 'gcloud/docker/add_tag.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/docker/add_tag.sh"

  set_variables() {
    export GCLOUD_PROJECT="my-project"
    export GCR_SRC_TAG="gcr.io/${GCLOUD_PROJECT}/my-image:tag"
    export GCR_DEST_TAG="gcr.io/${GCLOUD_PROJECT}/my-image:new-tag"
  }

  Before 'set_variables'

  It 'should fail when GCR_SRC_TAG env variable is not defined'
    unset GCR_SRC_TAG

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCR_SRC_TAG environment variable should be defined"
  End

  # ---

  It 'should fail when GCR_DEST_TAG env variable is not defined'
    unset GCR_DEST_TAG

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCR_DEST_TAG environment variable should be defined"
  End

  # ---

  It 'should success when adding a new tag to existing image'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "container images add-tag ${GCR_SRC_TAG} ${GCR_DEST_TAG}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
