# shellcheck shell=sh

Describe 'gcloud/release_ip_address.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/release_ip_address.sh"

  set_variables() {
    export GCLOUD_PROJECT="my-project"
    export GCLOUD_IP_ADDRESS_NAME="my-ip"
  }

  Before 'set_variables'

  It 'should fail when GCLOUD_PROJECT env variable is not defined'
    unset GCLOUD_PROJECT

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_PROJECT environment variable should be defined"
  End

  # ---

  It 'should fail when GCLOUD_IP_ADDRESS_NAME env variable is not defined'
    unset GCLOUD_IP_ADDRESS_NAME

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_IP_ADDRESS_NAME environment variable should be defined"
  End

  # ---

  It 'should success and release desired global ip address'
    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "compute addresses delete ${GCLOUD_IP_ADDRESS_NAME} --global --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should success and release desired regional ip address'
    export GCLOUD_IP_ADDRESS_REGION="europe-west1"

    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "compute addresses delete ${GCLOUD_IP_ADDRESS_NAME} --region=${GCLOUD_IP_ADDRESS_REGION} --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
