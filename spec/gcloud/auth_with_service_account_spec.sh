# shellcheck shell=sh

Describe 'gcloud/auth_with_service_account.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/auth_with_service_account.sh"

  set_variables() {
    export GCLOUD_KEY="my-private-key"
  }

  Before 'set_variables'

  It 'should fail when GCLOUD_KEY or GCLOUD_KEY_B64 environment variable is not defined'
    unset GCLOUD_KEY

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_KEY or GCLOUD_KEY_B64 environment variable should be defined"
  End

  # ---

  It 'should success when key file is given'
    export TMP_KEY_FILE="/${SHELLSPEC_TMPDIR}/key"

    Intercept begin
    __begin__() {
      rm() {
        touch "${SHELLSPEC_TMPDIR}/.${SHELLSPEC_EXAMPLE_ID}_rm_has_been_called"
      }

      mktemp() {
        echo "${TMP_KEY_FILE}"
      }

      gcloud() {
        case "$*" in
          "auth activate-service-account --key-file ${TMP_KEY_FILE}.json")
             echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
    The file "${SHELLSPEC_TMPDIR}/.${SHELLSPEC_EXAMPLE_ID}_rm_has_been_called" should be file
    rm -rf "${SHELLSPEC_TMPDIR}/.${SHELLSPEC_EXAMPLE_ID}_rm_has_been_called"
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
