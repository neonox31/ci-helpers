# shellcheck shell=sh

Describe 'gcloud/appengine/deploy_version.sh'
  CIH_SCRIPT="${CIH_PATH}/gcloud/appengine/deploy_version.sh"

  set_variables() {
    export GCLOUD_PROJECT="my-project"
    export GAE_VERSION="my-version"
    export GAE_DEPLOYABLES_PATH="dist/"
  }

  Before 'set_variables'

  It 'should fail when GCLOUD_PROJECT env variable is not defined'
    unset GCLOUD_PROJECT

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GCLOUD_PROJECT environment variable should be defined"
  End

  # ---

  It 'should fail when GAE_VERSION env variable is not defined'
    unset GAE_VERSION

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GAE_VERSION environment variable should be defined"
  End

  # ---

  It 'should fail when GAE_DEPLOYABLES_PATH env variable is not defined'
    unset GAE_DEPLOYABLES_PATH

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "GAE_DEPLOYABLES_PATH environment variable should be defined"
  End

  # ---

  It 'should success and use default service when service is not specified'
    export GAE_DEPLOYABLES_PATH="${SHELLSPEC_TMPDIR}/${SHELLSPEC_EXAMPLE_ID}"
    mkdir -p "${GAE_DEPLOYABLES_PATH}"

    echo "service: foo" > "${GAE_DEPLOYABLES_PATH}/app.yaml"

    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app deploy ${GAE_DEPLOYABLES_PATH} --version ${GAE_VERSION} --no-promote --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
    The file "${GAE_DEPLOYABLES_PATH}/app.yaml" contents should equal "service: default"

    rm -rf "${GAE_DEPLOYABLES_PATH}"
  End

  # ---

  It 'should success and use desired service when GAE_SERVICE environment variable is specified'
    export GAE_SERVICE="my-service"
    export GAE_DEPLOYABLES_PATH="${SHELLSPEC_TMPDIR}/${SHELLSPEC_EXAMPLE_ID}"
    mkdir -p "${GAE_DEPLOYABLES_PATH}"

    echo "service: default" > "${GAE_DEPLOYABLES_PATH}/app.yaml"

    Intercept begin
    __begin__() {
      gcloud() {
        case "$*" in
          "app deploy ${GAE_DEPLOYABLES_PATH} --version ${GAE_VERSION} --no-promote --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
    The file "${GAE_DEPLOYABLES_PATH}/app.yaml" contents should equal "service: my-service"

    rm -rf "${GAE_DEPLOYABLES_PATH}"
  End

  # ---

  It 'should success and and promote version if GAE_PROMOTE_VERSION envrionment variable is defined'
    export GAE_PROMOTE_VERSION="true"

    Intercept begin
    __begin__() {
      sed() {
        return 0
      }

      gcloud() {
        case "$*" in
          "app deploy ${GAE_DEPLOYABLES_PATH} --version ${GAE_VERSION} --promote --project ${GCLOUD_PROJECT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on gcloud error'
    Intercept begin
    __begin__() {
            sed() {
        return 0
      }

      gcloud() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
