# shellcheck shell=sh

Describe 'rocketchat/send_message.sh'
  CIH_SCRIPT="${CIH_PATH}/rocketchat/send_message.sh"

  set_variables() {
    export ROCKETCHAT_TOKEN="my-token"
    export ROCKETCHAT_URL="https://chat.rocket.com"
    export ROCKETCHAT_USER_ID="123abcdefgh"
    export ROCKETCHAT_CHANNEL="#my-chan"
    export ROCKETCHAT_MESSAGE="hello"
  }

  Before 'set_variables'

  It 'should fail when ROCKETCHAT_TOKEN env variable is not defined'
    unset ROCKETCHAT_TOKEN

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ROCKETCHAT_TOKEN or ROCKETCHAT_TOKEN_B64 environment variable should be defined"
  End

  # ---

  It 'should fail when ROCKETCHAT_URL env variable is not defined'
    unset ROCKETCHAT_URL

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ROCKETCHAT_URL environment variable should be defined"
  End

  # ---

  It 'should fail when ROCKETCHAT_USER_ID env variable is not defined'
    unset ROCKETCHAT_USER_ID

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ROCKETCHAT_USER_ID or ROCKETCHAT_USER_ID_B64 environment variable should be defined"
  End

  # ---

  It 'should fail when ROCKETCHAT_CHANNEL env variable is not defined'
    unset ROCKETCHAT_CHANNEL

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ROCKETCHAT_CHANNEL environment variable should be defined"
  End

  # ---

  It 'should fail when ROCKETCHAT_MESSAGE env variable is not defined'
    unset ROCKETCHAT_MESSAGE

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "ROCKETCHAT_MESSAGE environment variable should be defined"
  End

  # ---

  It 'should success and send a message using rocketchat'
    EXPECTED_JSON_INPUT=$(cat << EOF
{
    "channel": "${ROCKETCHAT_CHANNEL}",
    "text": "${ROCKETCHAT_MESSAGE}"
}
EOF
)

    Intercept begin
    __begin__() {
      curl() {
        case "$*" in
          "-qsSf -X POST ${ROCKETCHAT_URL}/api/v1/chat.postMessage -H Content-Type:application/json -H X-Auth-Token:${ROCKETCHAT_TOKEN} -H X-User-Id:${ROCKETCHAT_USER_ID} -d ${EXPECTED_JSON_INPUT}")
            echo "success"
        esac
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be success
    The output should equal "success"
  End

  # ---

  It 'should fail on curl error'
    Intercept begin
    __begin__() {
      curl() {
        echo "an error occured" 1>&2
        return 1
      }
    }

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should equal "an error occured"
  End
End
