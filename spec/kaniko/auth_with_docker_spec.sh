# shellcheck shell=sh

Describe 'kaniko/auth_with_docker.sh'
  CIH_SCRIPT="${CIH_PATH}/kaniko/auth_with_docker.sh"

  set_variables() {
    export DOCKER_PASSWORD="my-password"
    export DOCKER_USER="my-user"
  }

  Before 'set_variables'

  It 'should fail when DOCKER_PASSWORD env variable is not defined'
    unset DOCKER_PASSWORD

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "DOCKER_PASSWORD or DOCKER_PASSWORD_B64 environment variable should be defined"
  End

 # ---

  It 'should fail when DOCKER_USER env variable is not defined'
    unset DOCKER_USER

    When run source "${CIH_SCRIPT}"

    The status should be failure
    The error should include "DOCKER_USER environment variable should be defined"
  End

  # ---

  It 'should success and authenticate kaniko on public docker registry'
    _KANIKO_DOCKER_CONFIG_PATH="$(mktemp)"
    export _KANIKO_DOCKER_CONFIG_PATH
    EXPECTED_DOCKER_CONFIG=$(cat << EOF
{
  "auths": {
    "https://index.docker.io/v1/": {
      "auth": "bXktdXNlcjpteS1wYXNzd29yZA=="
    }
  }
}
EOF
)

    When run source "${CIH_SCRIPT}"

    The status should be success
    The file "${_KANIKO_DOCKER_CONFIG_PATH}" contents should equal "${EXPECTED_DOCKER_CONFIG}"
  End

  # ---

  It 'should success and authenticate kaniko on private docker registry'
    export DOCKER_REGISTRY="https://private-registry.docker.com"
    _KANIKO_DOCKER_CONFIG_PATH="$(mktemp)"
    export _KANIKO_DOCKER_CONFIG_PATH
    EXPECTED_DOCKER_CONFIG=$(cat << EOF
{
  "auths": {
    "https://private-registry.docker.com": {
      "auth": "bXktdXNlcjpteS1wYXNzd29yZA=="
    }
  }
}
EOF
)

    When run source "${CIH_SCRIPT}"

    The status should be success
    The file "${_KANIKO_DOCKER_CONFIG_PATH}" contents should equal "${EXPECTED_DOCKER_CONFIG}"
  End
End
